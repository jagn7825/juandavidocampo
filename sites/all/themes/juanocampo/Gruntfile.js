'use strict';
module.exports = function(grunt) {
  // cargamos todas las tareas
  require('load-grunt-tasks')(grunt);
  // Muestra tiempo transcurrido
  //require('time-grunt')(grunt);

  var jsFileList = [
    'assets/libs/scroll/perfect-scrollbar.js',
    'assets/js/_*.js',
    'assets/js/*/_*.js'
  ];

  // Configuracion del proyecto.
  grunt.initConfig({
    less: {
      comunes: {
        files: {
          "dist/css/comunes.css": [
            "assets/libs/scroll/_perfect-scrollbar.less",
            "assets/less/comunes.less"
          ]
        },
        options: {
          compress: false,
          //source maps
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'comunes.css.map',
          sourceMapFilename: 'dist/css/comunes.css.map'
        }
      },
      desktop: {
        files: {
          "dist/css/desktop.css": [
            "assets/less/desktop.less"
          ]
        },
        options: {
          compress: false,
          //source maps
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'desktop.css.map',
          sourceMapFilename: 'dist/css/desktop.css.map'
        }
      },
      mobile: {
        files: {
          "dist/css/mobile.css": [
            "assets/less/mobile.less"
          ]
        },
        options: {
          compress: false,
          //source maps
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'mobile.css.map',
          sourceMapFilename: 'dist/css/mobile.css.map'
        }
      },
      build: {
        files: {
          "dist/css/desktop.css": [
            "assets/less/desktop.less"
          ],
          "dist/css/comunes.css": [
            "assets/less/comunes.less"
          ],
            "dist/css/mobile.css": [
            "assets/less/mobile.less"
          ]
        },
        options: {
          compress: true
        }
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      libs:{
        src: [
          'assets/libs/scroll/perfect-scrollbar.js',
          'assets/libs/select2/select2.js'
        ],
        dest: 'dist/js/libs.js'
      },
      comunes:{
        src: ['assets/js/comunes/*.js'],
        dest: 'dist/js/comunes.js'
      },
      desktop: {
        src: ['assets/js/desktop/*.js'],
        dest: 'dist/js/desktop.js'
      },
      moviles: {
        src: ['assets/js/mobile/*.js'],
        dest: 'dist/js/mobile.js'
      }
    },
    uglify: {
      libs: {
        src: ['dist/js/libs.js'],
        dest: 'dist/js/libs.js'
      },
      comunes:{
        src: ['dist/js/comunes.js'],
        dest: 'dist/js/comunes.js'
      },
      desktop: {
        src: ['dist/js/desktop.js'],
        dest: 'dist/js/desktop.js'
      },
      moviles: {
        src: ['dist/js/mobile.js'],
        dest: 'dist/js/mobile.js'
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
      },
      dev: {
        options: {
          map: {
            prev: 'dist/css/'
          }
        },
        src: [
          'dist/css/comunes.css',
          'dist/css/desktop.css',
          'dist/css/mobile.css'
          ]
      },
      build: {
        src: ['dist/css/mobile.css',
             'dist/css/comunes.css',
             'dist/css/desktop.css'
             ]
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
          'assets/less/*/*.less'
        ],
        tasks: ['less:mobile','less:comunes','less:desktop','autoprefixer:dev']
      },
      js: {
        files: [
          jsFileList
        ],
        tasks: ['jshint','concat']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: false
        },
        files: [
          'assets/css/main.css',
          'assets/js/scripts.js'
        ]
      }
    },
    clean: {
      build: 'dist', 
      temp: 'dist/*'
    }
  });

  // resgistrar las tareas
  grunt.registerTask('default', [
    'dev'
  ]);

  grunt.registerTask('dev', [
    'less:mobile',
    'less:comunes',
    'less:desktop',
    'autoprefixer:dev',
    'concat'
  ]);

  grunt.registerTask('build', [
    'clean',
    'jshint',
    'less:build',
    'autoprefixer:build',
    'concat',
    'uglify'
  ]);
};